
# Amoba Calendar

## Requerimientos
- PHP 8.2
- Node v12.0 o superior

## Instalación
- Sacar copia al archivo ".env.example" y renombrar a ".env"
- Configure las variables de conexión a la base de datos
- **!Importante¡** sí usa un dominio para levantar la app, debe configurar la variable de entorno "APP_URL" con el dominio por ejemplo: "APP_URL=amoba-calendar.test"; y si usa laravel sail usar como "APP_URL=http://localhost"; ya que esta configuración se usa para poblar los recursos de los json de datos
- *<Opcional>* Si usa Laravel Sail usar estas variables de entorno adicionales: 
```bash
    APP_PORT=8801
    FORWARD_DB_PORT=8802
    PMA_PORT=8803
    VITE_PORT=8804
```
- Ejecutar el comando "composer install"
- Ejecutar el comando "php artisan migrate --seed"
- En caso de compilar nuevamente, ejecutar los comandos: "npm install" y "npm run dev"
