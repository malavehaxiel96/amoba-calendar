<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RouteData extends Model
{
    protected $table = "routes_data";

    protected $fillable = [
		"route_id",
		"calendar_id",
		"vinculation_route",
		"route_circular",
		"date_init",
		"date_finish",
		"mon",
		"tue",
		"wed",
		"thu",
		"fri",
		"sat",
		"sun",
    ];
}
