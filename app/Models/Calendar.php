<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Calendar extends Model
{
    protected $fillable = [
        'calendar_id', 
        'name',  
    ];

    protected $with = ['days_disabled'];

    public function days_disabled()
    {
        return $this->hasMany(CalendarDayDisabled::class);
    }
}
