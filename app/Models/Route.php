<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
    protected $fillable = [
        'external_id', 
        'invitation_code', 
        'title', 
        'start_timestamp', 
        'end_timestamp', 
    ];

    protected $with = ['route_datas'];

    public function route_datas()
    {
        return $this->hasMany(RouteData::class, 'route_id');
    }

    public function reservations()
    {
        return $this->hasMany(Reservation::class, 'route_id');
    }
}
