<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CalendarDayDisabled extends Model
{
    protected $table = "calendars_day_disabled";

    protected $fillable = [
        'calendar_id',
        'day',
        'enabled',
    ];
}
