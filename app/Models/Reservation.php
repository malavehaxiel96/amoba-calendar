<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $fillable = [
        "user_plan_id",
		"route_id",
		"track_id",
		"reservation_start",
		"reservation_end",
		"route_stop_origin_id",
		"route_stop_destination_id"
    ];

	protected $with = ['user_plan', 'route'];

	public function user_plan()
    {
        return $this->belongsTo(UserPlan::class);
    }

	public function route()
    {
        return $this->belongsTo(Route::class);
    }
}
