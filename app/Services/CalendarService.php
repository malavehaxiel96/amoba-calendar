<?php

namespace App\Services;

use App\Models\Route;
use App\Traits\CalendarAction;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CalendarService
{
    use CalendarAction;

    public function getCalendar(Request $request)
    {
        // Filters
        $calendar = $request->get('calendar');
        $date = $request->get('dates');
        $routeId = $request->get('route_id');
        $dates = $this->resolveDateRange($date);
        $start = $dates['start'];
        $end = $dates['end'];

        // manage calendar
        $calendar = $this->manageCalendar($calendar, $routeId, $start, $end);

        return response()->json([
            'code' => 200,
            'message' => "successfull",
            'error' => false,
            'data' => $calendar
        ]);
    }

    public function getRoutes(Request $request)
    {
        $date = $request->get('dates');
        $dates = $this->resolveDateRange($date);
        $start = $dates['start'];
        $end = $dates['end'];

        $routes = Route::when($start and $end, function($query) use ($start, $end){
            return $query->where('start_timestamp', '>=', $start)
                ->where('start_timestamp', '<=', $end)
                ->orWhere('end_timestamp', '>=', $start)
                ->where('end_timestamp', '<=', $end);
        })->get();

        return response()->json([
            'code' => 200,
            'message' => "successfull",
            'error' => false,
            'data' => $routes
        ]);
    }
}
