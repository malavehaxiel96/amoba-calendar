<?php

use Illuminate\Support\Facades\Http;

function readJson($file, $option = false)
{
    $rs = Http::get(url($file));

    return json_decode(json_encode($rs->json()), $option);
}
