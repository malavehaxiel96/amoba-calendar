<?php

namespace App\Traits;

use App\Models\CalendarDayDisabled;
use App\Models\Reservation;
use App\Models\RouteData;
use App\Models\Service;
use Carbon\Carbon;
use stdClass;

trait CalendarAction
{
    protected function resolveDateRange($date)
    {
        if (isset($date)) {
            $start = $date[0];
            $end = $date[1];
        }else {
            $start = null;
            $end = null;
        }

        return compact('start', 'end');
    }

    protected function getCalendarDaysDisabled($calendar, $start, $end)
    {
        return CalendarDayDisabled::where('enabled', true)
            ->when($calendar, function ($query) use ($calendar){
                return $query->where('calendar_id', $calendar);
            })
            ->when($start and $end, function($query) use ($start, $end){
                return $query->where('day', '>=', $start)
                    ->where('day', '<=', $end);
            })
            ->orderBy('day')
            ->get();
    }

    protected function getReservedDays($route, $start, $end)
    {
        return Reservation::
            when($route, function ($query) use ($route){
                return $query->where('route_id', $route);
            })
            ->when($start and $end, function($query) use ($start, $end){
                return $query->where('reservation_start', '>=', $start)
                    ->where('reservation_start', '<=', $end)
                    ->orWhere('reservation_end', '>=', $start)
                    ->where('reservation_end', '<=', $end);
            })
            ->orderBy('reservation_start')
            ->get();
    }

    protected function getServiceDays($start, $end)
    {
        return Service::
            when($start and $end, function($query) use ($start, $end){
                return $query->where('arrival_timestamp', '>=', $start)
                    ->where('arrival_timestamp', '<=', $end)
                    ->orWhere('departure_timestamp', '>=', $start)
                    ->where('departure_timestamp', '<=', $end);
            })
            ->orderBy('arrival_timestamp')
            ->get();
    }

    protected function getOutFrecuencyDays($route, $start, $end)
    {
        return RouteData::when($route, function ($query) use ($route){
            return $query->where('route_id', $route);
        })
            ->when($start and $end, function($query) use ($start, $end){
                return $query->where('date_init', '>=', $start)
                    ->where('date_init', '<=', $end)
                    ->orWhere('date_finish', '>=', $start)
                    ->where('date_finish', '<=', $end);
            })
            ->orderBy('date_init')
            ->get();
    }

    protected function manageCalendar($calendar, $routeId, $start, $end)
    {
        $calendarDayDisabled = $this->getCalendarDaysDisabled($calendar, $start, $end);
        $reservedDays = $this->getReservedDays($routeId, $start, $end);
        $serviceDays = $this->getServiceDays($start, $end);
        $outFrecuencyDays = $this->getOutFrecuencyDays($routeId, $start, $end);

        $collections = new stdClass();
        $collections->calendarDayDisabled = $calendarDayDisabled;
        $collections->reservedDays = $reservedDays;
        $collections->serviceDays = $serviceDays;
        $collections->outFrecuencyDays = $outFrecuencyDays;

        $start = new Carbon($start);
        $end = new Carbon($end);

        $startDate = new Carbon("first day of " . __('calendar.months.'.$start->month) . " {$start->year}");
        $endDate = new Carbon("last day of " . __('calendar.months.'.$end->month) . " {$end->year}");
        $auxStartDate = new Carbon("first day of " . __('calendar.months.'.$start->month) . " {$start->year}");

        $arr = [[
            'year' => $startDate->year,
            'month' => $startDate->month,
            'month_name' => __('calendar.months.' . $startDate->month),
            'days' => $this->getDaysAndEvents($startDate->year, $startDate->month, $collections),
            'show' => 'active']
        ];

        while ($auxStartDate < $endDate and $auxStartDate->month < $endDate->month) {
            $auxStartDate->addMonth();

            $arr[] = [
                'year' => $auxStartDate->year,
                'month' => $auxStartDate->month,
                'month_name' => __('calendar.months.' . $auxStartDate->month),
                'days' => $this->getDaysAndEvents($auxStartDate->year, $auxStartDate->month, $collections),
                'show' => 'none'
            ];
        }

        return $arr;
    }

    protected function getDaysAndEvents($year, $month, $collections)
    {
        $rs = array();

        foreach (range(1, cal_days_in_month(CAL_GREGORIAN, $month, $year)) as $i)
        {
            $auxMonth = str_pad($month, 2, "0", STR_PAD_LEFT);
            $auxDay = str_pad($i, 2, "0", STR_PAD_LEFT);
            $date = "{$year}-{$auxMonth}-{$auxDay}";
            $dateCarbon = new Carbon($date);

            // Info
            $calendarDayDisabled = $collections->calendarDayDisabled->where('day', $date)->count();

            $outFrecuencyDays = $collections->outFrecuencyDays->filter(function ($item) use ($date) {
                return $date >= $item->date_init
                and $date <= $item->date_finish;
            })->where(strtolower($dateCarbon->format('D')), 0)->count();

            $reservedDays = $collections->reservedDays->filter(function ($item) use ($date) {
                return $date >= $item->reservation_start
                and $date <= $item->reservation_end;
            })->count();

            $serviceDays = $collections->serviceDays->filter(function ($item) use ($date) {
                return $date >= $item->arrival_timestamp
                and $date <= $item->departure_timestamp;
            })->count();

            // Status
            if ($calendarDayDisabled > 0) $status = 'disabled_day';
            elseif ($outFrecuencyDays > 0) $status = 'out_frecuency_day';
            elseif ($reservedDays > 0) $status = 'reserved_day';
            elseif ($serviceDays > 0) $status = 'service_day';
            else $status = null;


            $rs[] = [
                'day' => $i,
                'position' => (new Carbon($date))->format('N'),
                'status' => $status
            ];
        }

        if (collect($rs)->first()['position'] != 7) {
            $daysNull = [];

            foreach (range(1, collect($rs)->first()['position']) as $index) {
                $daysNull[] = [
                    'day' => 0,
                    'disabled_days' => 0,
                    'days_out_frecuency' => 0,
                    'reserved_days' => 0,
                    'service_days' => 0,
                ];
            }

            $daysNull = array_reverse($daysNull);

            foreach ($daysNull as $dayNUll) {
                array_unshift($rs, $dayNUll);
            }
        }

        return array_chunk($rs, 7);
    }
}
