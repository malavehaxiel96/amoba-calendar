<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\CalendarDayDisabled;
use App\Services\CalendarService;
use App\Traits\CalendarAction;
use Illuminate\Http\Request;

class CalendarController extends Controller
{
    public $calendarService;

    public function __construct(CalendarService $calendarService)
    {
        $this->calendarService = $calendarService;
    }

    public function info(Request $request)
    {
        return $this->calendarService->getCalendar($request);
    }

    public function routes(Request $request)
    {
        return $this->calendarService->getRoutes($request);
    }
}
