<?php

namespace Database\Seeders;

use App\Models\Calendar;
use Illuminate\Database\Seeder;

class LoadCalendarsDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $calendars = readJson('data/calendar.json', true)['calendars'];

        foreach ($calendars as $value)
        {
            $calendar = new Calendar(collect($value)->except('id')->toArray());
            $calendar->save();
        }
    }
}
