<?php

namespace Database\Seeders;

use App\Models\Reservation;
use App\Models\Route;
use App\Models\UserPlan;
use Illuminate\Database\Seeder;

class LoadReservationsDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $reservations = readJson('data/reservations.json', true)['reservations'];

        foreach ($reservations as $value)
        {
            $reservation = new Reservation(collect($value)->except('id')->toArray());
            $reservation->save();
        }
    }
}
