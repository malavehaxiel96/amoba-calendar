<?php

namespace Database\Seeders;

use App\Models\Service;
use Illuminate\Database\Seeder;

class LoadServicesDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $services = readJson('data/services.json', true)['services'];

        foreach ($services as $value)
        {
            $service = new Service(collect($value)->except('id')->toArray());
            $service->save();
        }
    }
}
