<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LoadUserDataSeeder::class);
        $this->call(LoadRoutesDataSeeder::class);
        $this->call(LoadCalendarsDataSeeder::class);
        $this->call(LoadCalendarDaysDisabledDataSeeder::class);
        $this->call(LoadRouteDataDataSeeder::class);
        $this->call(LoadUserPlanDataSeeder::class);
        $this->call(LoadReservationsDataSeeder::class);
        $this->call(LoadServicesDataSeeder::class);
    }
}
