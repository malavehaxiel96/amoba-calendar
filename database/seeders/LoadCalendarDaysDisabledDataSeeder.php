<?php

namespace Database\Seeders;

use App\Models\Calendar;
use App\Models\CalendarDayDisabled;
use Illuminate\Database\Seeder;

class LoadCalendarDaysDisabledDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $calendarsDaysDisabled = readJson('data/calendar_days_disabled.json', true)['calendar_days_disabled'];

        foreach ($calendarsDaysDisabled as $value)
        {
            $calendarDayDisabled = new CalendarDayDisabled(collect($value)->except('id')->toArray());
            $calendarDayDisabled->save();
        }
    }
}
