<?php

namespace Database\Seeders;

use App\Models\Route;
use Illuminate\Database\Seeder;

class LoadRoutesDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $routes = readJson('data/routes.json', true)['routes'];

        foreach ($routes as $value)
        {
            $route = new Route(collect($value)->except('id')->toArray());
            $route->save();
        }
    }
}
