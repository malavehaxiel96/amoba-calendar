<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class LoadUserDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = readJson('data/users.json')->users;

        foreach ($users as $value)
        {
            $user = new User();
            $user->first_name = $value->first_name;
            $user->last_name = $value->last_name;
            $user->phone_number = $value->phone_number;
            $user->picture = $value->picture;
            $user->email = $value->email;
            $user->password = $value->password ?? bcrypt('12346578');
            $user->remember_token = $value->remember_token;
            $user->last_online = $value->last_online;
            $user->verification_code = $value->verification_code;
            $user->new_email = $value->new_email;
            $user->status = $value->status;
            $user->first = $value->first;
            $user->last_accept_date = $value->last_accept_date;
            $user->company_contact = $value->company_contact;
            $user->credits = $value->credits;
            $user->first_trip = $value->first_trip;
            $user->incomplete_profile = $value->incomplete_profile;
            $user->phone_verify = $value->phone_verify;
            $user->token_auto_login = $value->token_auto_login;
            $user->user_vertical = $value->user_vertical;
            $user->language_id = $value->language_id;
            $user->no_registered = $value->no_registered;
            $user->created = $value->created;
            $user->modified = $value->modified;
            $user->deleted_at = $value->deleted_at;
            $user->save();
        }
    }
}
