<?php

namespace Database\Seeders;

use App\Models\Calendar;
use App\Models\Route;
use App\Models\RouteData;
use Illuminate\Database\Seeder;

class LoadRouteDataDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $routesData = readJson('data/route_data.json', true)['routes_data'];

        foreach ($routesData as $value)
        {
            $routeData = new RouteData(collect($value)->except('id')->toArray());
            $routeData->save();
        }
    }
}
