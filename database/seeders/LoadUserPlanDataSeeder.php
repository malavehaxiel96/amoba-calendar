<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\UserPlan;
use Illuminate\Database\Seeder;

class LoadUserPlanDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userPlans = readJson('data/user_plans.json', true)['user_plans'];

        foreach ($userPlans as $value)
        {
            $userPlan = new UserPlan(collect($value)->except('id')->toArray());
            $userPlan->save();
        }
    }
}
